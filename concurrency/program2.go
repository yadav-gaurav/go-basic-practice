package main

import (
	"fmt"
	"runtime"
	"time"
)

func f1() {
	for index := 0; index < 5; index++ {
		fmt.Println("I am form the f1 !!!")
	}
}

func f2() {
	for index := 0; index < 5; index++ {
		fmt.Println("I am from the f2 !!!")
	}
}

func main() {
	go f1()
	time.Sleep(time.Second)
	fmt.Println("Execeution completed !!!!!!!!!!!!!!!")
	f2()
	fmt.Println("No of goroutines :", runtime.NumGoroutine())
	fmt.Println("Number of CPUs :", runtime.NumCPU())
	fmt.Println("Number of CPUs :", runtime.GOMAXPROCS(0))
}
