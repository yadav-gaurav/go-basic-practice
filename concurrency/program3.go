package main

import (
	"fmt"
	"sync"
	"time"
)

func f1(wg *sync.WaitGroup) {
	for index := 0; index < 5; index++ {
		fmt.Println("I am form function f1 !!!!")
		time.Sleep(time.Second * 5)
	}
	wg.Done()
}
func f2() {
	for index := 0; index < 5; index++ {
		fmt.Println("I am from the function f2 !!!!")
	}
}
func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	go f1(&wg)
	wg.Wait()
	f2()
}
