package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func apiCall(url string) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		fmt.Printf("%s is Down !\n", url)
	} else {
		defer resp.Body.Close()
		fmt.Println("Status code :", resp.StatusCode)
		if resp.StatusCode == 200 {
			respByte, error := ioutil.ReadAll(resp.Body)
			if error != nil {
				fmt.Println(error)
			} else {
				fmt.Println(respByte)
			}
		}
	}
}
func main() {
	apiCall("https://jsonplaceholder.typicode.com/users")
}
