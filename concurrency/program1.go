package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("Main execution started !!!")
	fmt.Println("No of CPUs :", runtime.NumCPU())
	fmt.Println("No of gorotuins :", runtime.NumGoroutine())
	fmt.Println("OS :", runtime.GOOS)
	fmt.Println("ARCHS :", runtime.GOARCH)
	fmt.Println("GOMAXPROCESS :", runtime.GOMAXPROCS(0))
}
