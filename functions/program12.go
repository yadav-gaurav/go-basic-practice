package main

import (
	"fmt"
	"time"
)

type names []string

func (n names) print() {
	for _, name := range n {
		fmt.Println(name)
	}
}
func main() {
	const day = 24 * time.Hour
	fmt.Printf("%T\n", day)
	second := day.Seconds()
	fmt.Println(second)

	employees := names{"yadav gaurav", "deepak yadav", "ravi yadav", "ashu yadav", "devendra yadav"}
	employees.print()
	// other way
	fmt.Println("********************Other way*********************888")
	names.print(employees)

}
