package main

import (
	"fmt"
)

type car struct {
	brand string
	color string
	price float64
}

func chnageCar1(c car, newBrand string, newColor string, price float64) {
	c.brand = newBrand
	c.color = newColor
	c.price = price
}
func (c car) changeCar2(newBrand string, newColor string, price float64) {
	c.brand = newBrand
	c.color = newColor
	c.price = price
}
func (c *car) chnageCar3(newBrand string, newColor string, price float64) {
	(*c).brand = newBrand
	(*c).color = newColor
	(*c).price = price
}
func main() {
	audi := car{"XUV", "red", 14123}
	fmt.Println(audi)
	chnageCar1(audi, "RXF", "black", 43433)
	fmt.Println(audi)
	audi.changeCar2("RXF", "black", 43433)
	fmt.Println(audi)
	// using pointer reciver methods
	fmt.Println("***************pointer reciever*************")
	(&audi).chnageCar3("RXF", "black", 43433)
	fmt.Println(audi)
}
