package main

import "fmt"

func swap(a,b *int){
var c int
c=*a
*a=*b
*b=c
}
func main(){
	var a int=3
	var b int=4
	swap(&a,&b)
	fmt.Printf("a is %d and b is %d \n",a,b)
}