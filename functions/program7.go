package main

import "fmt"

func myfunc(p,q int)(int,int,int,int){
	return p-q, p+q, p*q, p/q
}

func main(){
	var add,sub,mul,div =myfunc(5,2)
	fmt.Println("Add :",add)
	fmt.Println("Sub :",sub)
	fmt.Println("mul :",mul)
	fmt.Println("Div :",div)
}