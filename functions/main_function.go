package main

import (
	"sort"
	"fmt"
	"strings"
	"time"
)

func main(){
// slice
numberSlice :=[]int{12,11,87,34,23,13}
sort.Ints(numberSlice)
fmt.Println("Sorted slice :",numberSlice)
//index
fmt.Println("Index value",strings.Index("Yadav Gaurav","Gaurav"))
//finding time
fmt.Println("Time :",time.Now())
}