package main

import "fmt"

func myfunc(a,b int)(add int,sub int,mul int,div int){
	add=a-b
	sub=a-b
	mul=a*b
	div=a/b
	return
}

func main(){
	add,sub,mul,div:=myfunc(6,2)
	fmt.Println("Add :",add)
	fmt.Println("Sub :",sub)
	fmt.Println("Mul :",mul)
	fmt.Println("Div :",div)
}