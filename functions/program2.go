package main

import "fmt"

func swap(a, b int){
	var c int=a
	a=b
	b=a
	a=c
}
func main(){
	var a int=3
	var b int=4
	swap(a,b)
	fmt.Printf("a is %d and b is %d \n",a,b)
}