package main

import "fmt"

func main() {
	var ch chan int
	fmt.Println(ch)
	// channel intialization
	ch = make(chan int)
	fmt.Println(ch)

	// dec and intialize both(send & recieve)
	c := make(chan int)
	fmt.Println(c)

	// send
	c <- 10
	// recieve
	num := <-c
	fmt.Println(num)

	// channel for send
	ch1 := make(chan<- string)

	// for recieve
	ch2 := make(<-chan string)
	fmt.Println(ch1, ch2)

}
