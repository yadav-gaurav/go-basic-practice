package main

import "fmt"

func f1(n int, ch chan int) {
	ch <- n
}
func main() {

	// channel intialization and dec for both recieve and send
	ch := make(chan int)
	go f1(10, ch)
	num := <-ch
	fmt.Println("value recieved :", num)
	fmt.Println("Exiting from the main")
}
