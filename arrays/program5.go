package main

import (
	"fmt"
)

func main(){
	number_array :=[...]int{1,2,3,4,5}
	copy_number_array :=number_array
	fmt.Println("Original :",number_array)
	fmt.Println("Copied :",copy_number_array)
	

	// after modification
	copy_number_array[1]=21212
	fmt.Println("Original :",number_array)
	fmt.Println("Copied :",copy_number_array)
}