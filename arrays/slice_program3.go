package main

import "fmt"

func main(){
	// slice using array
	numberArray :=[...]int{1,2,3,4,56,7,8}
	slice1 :=numberArray[0:5]
	slice2 :=numberArray[0:]
	slice3 :=numberArray[:6]
	slice4 :=numberArray[:]
	fmt.Println("slice1 :",slice1)
	fmt.Println("slice2 :",slice2)
	fmt.Println("slice3 :",slice3)
	fmt.Println("slice4 :",slice4)
}