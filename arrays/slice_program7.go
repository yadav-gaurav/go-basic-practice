package main

import (
	"sort"
	"fmt"
)

func main(){
	// creating array
	// number_array :=[...]int{1,8,5,6,3,4,2,1}
	// sort array can not do
	// sort.Ints(number_array)

	// creating slice
	number_slice :=[]int{1,8,5,6,3,4,2,1}
	fmt.Println("original slice :",number_slice)
	sort.Ints(number_slice)
	fmt.Println("sorted slice :",number_slice)

}