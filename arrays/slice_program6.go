package main

import "fmt"

func main(){
	number_slice :=[]int{1,2,3,4,5,5,6,7,8,9}
	for i:=0;i<len(number_slice);i++{
		fmt.Print(number_slice[i]," ")
	}
	fmt.Print("\n")

	// using range
    for index,num :=range number_slice{
		fmt.Print(num," index :",index," ")
	}
	fmt.Println()

}