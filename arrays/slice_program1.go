package main

import "fmt"

func main(){
	// creating array
	arr :=[9]int{1,2,3,4,5,6,7,8,9}

	//display array
	fmt.Println("Array :",arr)

	// creating slice
	sliceArray :=arr[3:7]

	// displaying slice array
	fmt.Println("Slice array :",sliceArray)

	// length
	fmt.Println("length of the sliceArray :",len(sliceArray))
	fmt.Println("capacity of SliceArray :",cap(sliceArray))
}