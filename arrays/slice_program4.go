package main

import "fmt"

func main(){
	number_array :=[...]int{1,2,3,4,5,6,7,8,9}

	// creating new slice
	main_slice :=number_array[1:8]

	// slice of slice
	slice1 :=main_slice[0:]
	slice2 :=main_slice[:4]
	slice3 :=main_slice[:]
	fmt.Println("slice1 :",slice1)
	fmt.Println("slice2 :",slice2)
	fmt.Println("slice3 :",slice3)
}