package main

import "fmt"

func main() {
	// numberSlice := []int{}
	// for i := 0; i < 6; i++ {
	// 	append(numberSlice, i)
	// }
	// fmt.Println(numberSlice)

	name := "yadav"
	fmt.Println(len(name))
	for _, char := range name {
		fmt.Printf("%c", char)
		fmt.Println()
	}
	fmt.Println("+++++++++++++++++++++++++++++++")
	for i := 0; i < len(name); i++ {
		fmt.Printf("%c \n", name[i])
	}
}
