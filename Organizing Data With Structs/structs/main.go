package main

import (
	"fmt"
	"strings"
)

type contactInfo struct {
	email   string
	zipCode int
}
type person struct {
	firstName string
	lastName  string
	contact   contactInfo
}

func main() {
	// first type
	var alex person
	alex.firstName = "Alex"
	alex.lastName = "Andesrson"
	alex.contact.email = "alex@gmail.com"
	alex.contact.zipCode = 23452

	// second way
	jim := person{
		firstName: "Jim",
		lastName:  "Party",
		contact: contactInfo{
			email:   "jim@gmail.com",
			zipCode: 34242,
		},
	}
	fmt.Println(alex)
	fmt.Printf("%+v\n", alex)
	fmt.Println(strings.Repeat("*", 30))
	fmt.Println(jim)
	fmt.Printf("%+v\n", jim)

}
