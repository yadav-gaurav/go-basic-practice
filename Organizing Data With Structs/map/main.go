package main

import "fmt"

func main() {
	// var letterMap map[string]int
	letterMap := map[string]int{}
	var name string
	fmt.Println("Enter a string!!!")
	fmt.Scanln(&name)
	for _, char := range name {
		ch, OK := letterMap[string(char)]
		if OK != false {
			letterMap[string(char)] = ch + 1
		} else {
			letterMap[string(char)] = 1
		}
	}
	fmt.Println(letterMap)
	fmt.Println(letterMap["a"])
}
