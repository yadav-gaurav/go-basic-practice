package main

import "fmt"

type wish interface {
	greet() string
}

type indianGreet struct{}
type usGreet struct{}

func (i indianGreet) greet() string {
	return "Hi Welcome!!"
}

func (u usGreet) greet() string {
	return "Hola!!!"
}

func print(w wish) {
	fmt.Println(w.greet())
}
func main() {
	in := indianGreet{}
	us := usGreet{}
	print(in)
	print(us)
}
