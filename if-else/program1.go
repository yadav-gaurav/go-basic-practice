package main

import (
	"fmt"
	"strconv"
)

func main() {
	if num, err := strconv.Atoi("89p"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(num)
	}
}
