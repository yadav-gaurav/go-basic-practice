package main

import "fmt"

func main(){
	var num int=23
	var p *int
	p=&num
	fmt.Printf("num is %d \n",num)
	fmt.Printf("Address of num is %d \n",&num)
	fmt.Printf("Value of p is %d \n",p)
	fmt.Printf("Address of p is %d \n",&p)
}