package main

import (
	"fmt"
	"bytes"
)

func main(){
	var b bytes.Buffer
	b.WriteString("Y")
	b.WriteString("A")
	b.WriteString("D")
	b.WriteString("A")
	b.WriteString("V")
	fmt.Println("String :",b.String())

	b.WriteString(" ")
	b.WriteString("G")
	b.WriteString("A")
	b.WriteString("U")
	b.WriteString("R")
	b.WriteString("AV")
	fmt.Println("String :",b.String())
}