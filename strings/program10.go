package main

import (
	"fmt"
	"strings"
)

func main(){
	str :="            @@@@heyyadav!gaurav&what%are@ doing..???"
	trim_results :=strings.Trim(str,"@?")
	fmt.Println(trim_results)
	fmt.Println("Trim left :",strings.TrimLeft(str,"@"))
	fmt.Println("Trim Right :",strings.TrimRight(str,"?"))
	fmt.Println("Trim Space :",strings.TrimSpace(str))
	name :="Yadav Gaurav"
	fmt.Println("Trim suffix :",strings.TrimSuffix(name,"Gaurav"))
	fmt.Println("Trim prefix :",strings.TrimPrefix(name,"Yadav"))
}