package main

import (
	"fmt"
	"strings"
)

func main(){
	name :="Welcome to yadav gaurav"
	fmt.Println("yadav is present or not :",strings.Contains(name,"yadav"))
	fmt.Println("Yadav is present or not :",strings.Contains(name,"Yadav"))

	// ConatinsAny() - check basis on unicode
	fmt.Println("yadav is present or not :",strings.ContainsAny(name,"yadav"))
	fmt.Println("Yadav is present or not :",strings.ContainsAny(name,"Yadav"))
}