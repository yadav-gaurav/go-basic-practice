package main

import (
	"fmt"
	"reflect"
)

type Author struct{
	name,lastName,langauage string
	practicle int
}

func main(){
	a1 :=Author{"gaurav","yadav","python",23}
	a2 :=Author{"mona","singh","java",23}
	a3 :=Author{"gaurav","yadav","python",23}
	fmt.Println(reflect.DeepEqual(a1,a2))
	fmt.Println(reflect.DeepEqual(a1,a3))
	fmt.Println(reflect.DeepEqual(a2,a3))
}