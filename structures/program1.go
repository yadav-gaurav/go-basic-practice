package main

import "fmt"

type Address struct{
	Name string
	city string
	Pincode int
}

func main(){
	var a Address
	fmt.Println("Value struct variable :",a)

	// Initialization
	a1:=Address{"Yadav Gaurav","Jaunpur",23344}
	fmt.Println(a1,a1.Name)
	
	// Second way of Intialization
	a2:=Address{Name:"Ashu Yadav",city:"Jaunpur"}
	fmt.Println(a2)
}