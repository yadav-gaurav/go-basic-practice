package main

func main() {
	cards := newDeck()
	cards.saveToFile("card-deck")
	myNewDecks := newDeckFromFile("card-deck")
	myNewDecks.shuffle()
	myNewDecks.print()

}
