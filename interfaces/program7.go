package main

import (
	"fmt"
	"math"
)

type Tank interface {
	Tarea() float64
	Volume() float64
}
type myValue struct {
	radius float64
	height float64
}

func (val myValue) Tarea() float64 {
	return 2*math.Pi*math.Pow(val.radius, 2) + 2*val.radius*val.height
}
func (val myValue) Volume() float64 {
	return math.Pi * math.Pow(val.radius, 2) * val.height
}
func Shape(t Tank) {
	fmt.Println(t.Tarea(), t.Volume())
}
func main() {
	dt := myValue{2.3, 5.0}
	Shape(dt)
}
