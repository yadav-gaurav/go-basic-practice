package main

import (
	"fmt"
	"math"
)

type shape1 interface {
	area() float64
}
type shape2 interface {
	perimeter() float64
}

// interface embedding
type shape interface {
	shape1
	shape2
	volume() float64
}

type circle struct {
	radius float64
}

func (c circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}
func (c circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}
func (c circle) volume() float64 {
	return 4 / 3 * math.Pi * math.Pow(c.radius, 3)
}
func printShape(s shape) {
	fmt.Println("Area :", s.area())
	fmt.Println("Permiter :", s.perimeter())
	fmt.Println("Volume :", s.volume())
}
func main() {
	circle := circle{2.3}
	printShape(circle)
}
