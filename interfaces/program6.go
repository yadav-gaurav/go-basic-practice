package main

import "fmt"

type emptyInterface interface {
}

func main() {
	var empty emptyInterface
	empty = 4
	fmt.Println("value assigined to empty interface :", empty)
	// dyanmic value to empty interface
	empty = "Yadav Gaurav"
	fmt.Println(empty)
}
