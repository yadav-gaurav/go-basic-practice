package main

import (
	"fmt"
	"math"
)

type shape interface {
	area()
}

func area(val int) {
	fmt.Println(math.Pi * 2.4)
	return
}

func Print(sh shape) {
	fmt.Println(sh.area())
	return
}

func main() {
	var sh shape
	Print(sh)
}
