package main

import (
	"fmt"
	"math"
)

type shape interface {
	area() float64
	perimeter() float64
}
type rectange struct {
	width, height float64
}
type circle struct {
	radius float64
}

func (c circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}
func (c circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}
func (r rectange) area() float64 {
	return r.width * r.height
}
func (r rectange) perimeter() float64 {
	return 2 * (r.width + r.height)
}
func print(s shape) {
	fmt.Println(s.area())
	fmt.Println(s.perimeter())
}
func main() {
	var s shape
	fmt.Printf("Interface shape :%T\n", s)
	circle := circle{3.}
	s = circle
	fmt.Printf("Interface type :%T\n", s)
}
