package main

import "fmt"

type tank interface{
	tankArea() float64
	tankVolume() float64
}
type myValue struct{
	radious float64
	height float64
}
// implementing methods of interfaces
func (m myValue) tankArea()float64{
	return 2*m.radious*m.height+2*3.14*m.radious*m.radious
}
func (m myValue) tankVolume()float64{
	return 3.14*m.radious*m.radious
}
func main(){
var t tank
t=myValue{10,14}
fmt.Println("Area :",t.tankArea())
fmt.Println("volume :",t.tankVolume())
}