package main

import (
	"fmt"
	"math"
)

type rectange struct {
	width, height float64
}
type circle struct {
	radius float64
}

func (c circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}
func (c circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}
func (r rectange) area() float64 {
	return r.width * r.height
}
func (r rectange) perimeter() float64 {
	return 2 * (r.width + r.height)
}
func printCircle(c circle) {
	fmt.Println("Shape of circle :", c)
	fmt.Println("Area of circle :", c.area())
	fmt.Println("Perimeter of circle :", c.perimeter())
}
func printRectangle(r rectange) {
	fmt.Println("Shape of rectangle :", r)
	fmt.Println("Area of rectangle :", r.area())
	fmt.Println("Perimeter of rectangle :", r.perimeter())
}
func main() {
	circle := circle{3.}
	rectange := rectange{2., 4.2}
	printCircle(circle)
	printRectangle(rectange)
}
